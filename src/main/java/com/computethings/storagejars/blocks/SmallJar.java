package com.computethings.storagejars.blocks;

public class SmallJar extends Jar {
    private static final SHAPE = VoxelShapes.join(Block.box(6, 0, 6, 10, 5, 10), 
                                                  Block.box(7, 5, 7, 9, 6, 9), 
                                                  IBooleanFunction.OR)

    public SmallJar() {
        super(PROPERTIES);

    @Override
    public VoxelShape getShape() {
        return SHAPE;
    }

    @Override
    public String getName() {
        return "small_jar";
    }

}

