package com.computethings.storagejars.blocks;

import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public abstract class Jar extends Block {
    public static final BlockBehaviour.Properties PROPERTIES = 
        BlockBehaviour.Properties.of(Material.GLASS).strength(0.3f).noOcclusion();

    public static final CreativeModeTab TAB = CreativeModeTab.TAB_MISC;

    public abstract String getName();

    public abstract VoxelShape getShape();

    @Override
    public VoxelShape getShape(BlockState state,
                               BlockGetter getter, 
                               BlockPos pos, 
                               CollisionContext context) {
        return this.getShape();
    }
}

