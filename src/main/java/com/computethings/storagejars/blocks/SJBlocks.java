package com.computethings.storagejars.blocks;

import static com.computethings.storagejars.StorageJars.MODID;
import com.computethings.storagejars.items.SJItems;

import net.minecraft.world.item.*;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.*;

import java.util.function.Supplier;

public class SJBlocks {
    public static final DeferredRegister<Block> BLOCKS = 
        DeferredRegister.create(ForgeRegistries.BLOCKS, MODID);

    private static <T extends Block> RegistryObject<T> registerBlockWithItem(String name,
                                                                             Supplier<T> block,
                                                                             CreativeModeTab tab) {
        RegistryObject<T> registeredBlock = BLOCKS.register(name, block);

        //register the blockItem for this block
        SJItems.ITEMS.register(name, 
            () -> new BlockItem(registeredBlock.get(), new Item.Properties().tab(tab)));

        return registeredBlock;
    }

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
    }
}

